package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Animal;
import com.example.demo.Model.Cat;
import com.example.demo.Model.Dog;

@Service
public class AnimalService {

    Dog dog1 = new Dog("corgi");
    Dog dog2 = new Dog("bull");
    Dog dog3 = new Dog("lap xuong");

    Cat cat1 = new Cat("mèo tam thể");
    Cat cat2 = new Cat("mèo ba tư");
    Cat cat3 = new Cat("mèo mỹ");

    public ArrayList<Animal> getAnimalList(){
        
        ArrayList<Animal> all = new ArrayList<>();

        all.add(dog1);
        all.add(dog2);
        all.add(dog3);
        all.add(cat1);
        all.add(cat2);
        all.add(cat3);
        
        return all ;
    }

}
